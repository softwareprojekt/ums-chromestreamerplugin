/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.totv;

import com.sun.jna.Platform;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import net.pms.PMS;
import net.pms.io.OutputParams;

import net.pms.io.ProcessWrapperImpl;

/**
 *
 * @author anton
 */
public class FFMPEGScreenVideo {
    
    private ProcessWrapperImpl pw;
    
    private  String FILENAME = "/tmp/ums/ffmpeg.avi"; 
    
    private final List<String> videotranscodeoptions;
    private final List<String> videobitrateoptions;
    private final List<String> audiobitrateoptions;
    
    public FFMPEGScreenVideo(){
        this.videotranscodeoptions = new LinkedList<>();
        this.videobitrateoptions = new LinkedList<>();
        this.audiobitrateoptions = new LinkedList<>();
        
    }
    
    public void setFilename(String add){
        this.FILENAME = add;
    }
    
    public void setVideoTranscodeOptions(List<String> options){
        this.videotranscodeoptions.addAll(options);
    }
    public void setVideoBitrateOptions(List<String> options){
        this.videobitrateoptions.addAll(options);
    }
    public void setAudioBitrateOptions(List<String> options){
        this.audiobitrateoptions.addAll(options);
    }
    
    public String executable() {
	return PMS.getConfiguration().getFfmpegPath();
    }
    
    
    public void start(){
        List<String> cmdList = new ArrayList<>();

        int nThreads = 0;


        cmdList.add(executable());


        // Prevent FFmpeg timeout
        cmdList.add("-y");

        cmdList.add("-loglevel");

        cmdList.add("debug");

        //cmdList.add("debug");
        cmdList.add("-s");
        cmdList.add("1920x1080");

        cmdList.add("-framerate");
        cmdList.add("25");

        cmdList.add("-f");
        if (Platform.isWindows()) {
                cmdList.add("dshow");
        } else {
                cmdList.add("x11grab");
        }

        // Decoder threads
        if (nThreads > 0) {
                cmdList.add("-threads");
                cmdList.add(String.valueOf(nThreads));
        }

        cmdList.add("-i");
        if (Platform.isWindows()) {
                cmdList.add("video=\"UScreenCapture\":audio=\"Microphone\"");
        } else {
                cmdList.add(":0.0");
        }

        // Encoder threads
        if (nThreads > 0) {
                cmdList.add("-threads");
                cmdList.add(String.valueOf(nThreads));
        }
        cmdList.add("-ar");
        cmdList.add("44100");

        if (!Platform.isWindows()) {
                cmdList.add("-f");
                cmdList.add("alsa");

                cmdList.add("-i");
                cmdList.add("pulse");
        }

        //cmdList.addAll(this.videotranscodeoptions);
        //cmdList.addAll(this.videobitrateoptions);
        //cmdList.addAll(this.audiobitrateoptions);
        
        //cmdList.add("-movflags");
        //mdList.add("+faststart");
        
        
        
        cmdList.add(this.FILENAME);

        OutputParams params = new OutputParams(PMS.getConfiguration());

        String[] cmdArray = new String[cmdList.size()];
        cmdList.toArray(cmdArray);

        for(String ar: cmdArray){
            System.out.print(ar+" ");
        }
        System.out.println();
        ProcessWrapperImpl pw_loc = new ProcessWrapperImpl(cmdArray, params);

        try {
                Thread.sleep(50);
        } catch (InterruptedException e) {
        }

        pw_loc.runInNewThread();

        System.out.println("SCREENCAST TO TV: "+pw_loc.toString());

        this.stop();
        

        this.pw = pw_loc;
    }
    
    public void stop(){
        if( pw != null && this.pw.isAlive()){
            this.pw.stopProcess();
        }
        pw = null;
        
    }
    
}
