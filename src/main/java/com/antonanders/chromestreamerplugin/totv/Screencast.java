/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.totv;
import ch.qos.logback.classic.util.ContextInitializer;
import com.antonanders.chromestreamerplugin.screenrecorder.ScreenRecorderVideo;
import com.antonanders.chromestreamerplugin.screenrecorder.ScreenRecorderVideoMPEG4;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.pms.configuration.RendererConfiguration;
import net.pms.dlna.DLNAMediaAudio;
import net.pms.dlna.DLNAMediaInfo;
import net.pms.dlna.DLNAResource;
import net.pms.dlna.Range;
import net.pms.dlna.RealFile;
import net.pms.encoders.FFMpegVideo;
import net.pms.encoders.Player;
import net.pms.formats.Format;
import org.slf4j.LoggerFactory;
/**
 *
 * @author anton
 */
public class Screencast extends RealFile {

    
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(DLNAResource.class);
    
    private static final String FILE_LOCATION = "/tmp/ums/";
    
    private File file;
    private FFMPEGScreenVideo ffmpeg;
    
    public Screencast(){
        this(new File(Screencast.FILE_LOCATION + "Screencast.mpeg"), "Screencast");
    }
    
    public Screencast(File file){
        this(file, "Screencast");
    }
    // -bufsize 1835000 -maxrate 499000000 -g 5 -q:v 1 -qmin 2 -qmax 3 -ac 2 -ab 640k -c:a ac3 -c:v mpeg2video -f vob
    
    public Screencast(File file, String name) {
        
        super(file, name);
        this.file = file;
        
    }

    @Override
    public void setPlayer(Player player) {
        super.setPlayer(player); //To change body of generated methods, choose Tools | Templates.
        LOGGER.info("PLAYER SET: "+player.toString());
        //FMpegVideo p = (FFMpegVideo) player;
        //this.ffmpeg.setVideoTranscodeOptions(p.getVideoTranscodeOptions(this, this.getMedia(), null) );
        //this.ffmpeg.setVideoBitrateOptions(p.getVideoBitrateOptions(this, this.getMedia(),null) );
        //this.ffmpeg.setAudioBitrateOptions(p.getAudioBitrateOptions(this, this.getMedia(),null) );
        this.getMedia().setContainer("mpegts");
        this.getMedia().setCodecV("mpeg2video");
        this.getMedia().setDuration(60*60d);
    }
    
    

    public boolean init(){
        //this.getMedia().setDuration(60*45d);
        LOGGER.info("SET MEDIA MPEG4");
        
        LOGGER.info("INIT ffmpegScreenCaster");
        this.ffmpeg = new FFMPEGScreenVideo();
        this.ffmpeg.setFilename(file.getAbsolutePath());
        
        //this.resolve();
        this.resolveFormat();
        
        
        return true;
    }
    
    @Override
    public boolean isValid() {
        return true;
    }
    
    
    
    private boolean reinitFile(){
        boolean fileexist = true;
        
        LOGGER.info("reinitFile");
        
        try {
            LOGGER.info("check dir");
            File verzeichnis = new File(Screencast.FILE_LOCATION);
            if(!verzeichnis.exists()){
                LOGGER.info("create DIR");
                verzeichnis.mkdir();
            }
            
            /*if(this.file.exists()){
                fileexist = false;
                LOGGER.info("delete "+this.file.getAbsolutePath());
                this.file.delete(); 
            }*/
            LOGGER.info("create "/*+ file.getAbsolutePath()*/);
            return this.file.createNewFile();
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage());
        }
        return false;
    }
    
    private void deleteFile(){
        if(file.isFile() && file.exists()){
            file.delete(); 
        } 
    }

    
    
    @Override
    public void startPlaying(String rendererId) {
        //this.reinitFile(this.file);
        
        super.startPlaying(rendererId); //To change body of generated methods, choose Tools | Templates.
    }

/*
    @Override
    public InputStream getInputStream(Range range, RendererConfiguration mediarenderer) throws IOException {
    LOGGER.info("getInputStream 2");
    //this.deleteFile();
    LOGGER.info("START screencast in FFMPEG");
    ffmpeg.start();
    LOGGER.info("SLEEP 2sek");
    try {
    Thread.sleep(5000);
    } catch (InterruptedException ex) {
    Logger.getLogger(Screencast.class.getName()).log(Level.SEVERE, null, ex);
    }
    LOGGER.info("SUPER.getInputStream()");
    return super.getInputStream(range, mediarenderer); //To change body of generated methods, choose Tools | Templates.
    }*/
    
    /*
    @Override
    public boolean isResumeable() {
        return false;
    }
    */
    
    @Override
    public long length() {
        return DLNAMediaInfo.TRANS_SIZE;
    }

    
    
    
    @Override
    public void stopPlaying(String rendererId) {
        super.stopPlaying(rendererId); //To change body of generated methods, choose Tools | Templates.
        ffmpeg.stop();
        file.delete();
    }
    
    
    
    private void selfMPEG4(){
        //this.setPlayer(new ScreenRecorderVideoMPEG4());
        LOGGER.info("SET MEDIA MPEG4");
        
        DLNAMediaInfo fakemedia = new DLNAMediaInfo();
        DLNAMediaAudio audio = new DLNAMediaAudio();
        fakemedia.setCodecV("mpeg4");
        fakemedia.setContainer("avi");
        fakemedia.setDuration(45d*60);
        fakemedia.setWidth(1920);
        fakemedia.setHeight(1080);
        fakemedia.setFrameRate("25");
        
        fakemedia.getAudioTracksList().add(audio);
        
        audio.setCodecA("mp3");
        audio.setSampleFrequency("44100");
        audio.getAudioProperties().setNumberOfChannels(2);
        
        
        this.setMedia(fakemedia);
        this.setMediaAudio(audio);
    }
    
    private void selfH264(){
        this.setPlayer(new ScreenRecorderVideo());
        
        DLNAMediaInfo fakemedia = new DLNAMediaInfo();
        DLNAMediaAudio audio = new DLNAMediaAudio();
        fakemedia.setCodecV("x264");
        fakemedia.setContainer("flv");
        fakemedia.setDuration(45d*60);
        fakemedia.setWidth(800);
        fakemedia.setHeight(480);
        fakemedia.setFrameRate("30");
        
        fakemedia.getAudioTracksList().add(audio);
        
        audio.setCodecA("mpga");
        audio.setSampleFrequency("44100");
        audio.getAudioProperties().setNumberOfChannels(2);
        
        
        this.setMedia(fakemedia);
        this.setMediaAudio(audio);
    }
    
    
}
