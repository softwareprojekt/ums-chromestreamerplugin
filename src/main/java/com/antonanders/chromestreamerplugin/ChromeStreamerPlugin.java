package com.antonanders.chromestreamerplugin;

import com.antonanders.chromestreamerplugin.screenrecorder.RTSPScreenStream;
import com.antonanders.chromestreamerplugin.screenrecorder.ScreenStream;
import javax.swing.JComponent;

import net.pms.dlna.DLNAResource;
import net.pms.dlna.WebVideoStream;


import net.pms.external.*;

/**
 *
 * @author anton
 */


public class ChromeStreamerPlugin implements AdditionalFolderAtRoot{

    public WebVideoStream root;
    public RTSPScreenStream appstream;
    private final Controller controller ;
    
    public ChromeStreamerPlugin(){
      
        controller = new Controller();
        
        appstream = new RTSPScreenStream();
        appstream.start();

    }
    
    @Override
    public DLNAResource getChild() {
        return controller.getStream();
    }

    @Override
    public JComponent config() {
        return null;
    }

    @Override
    public String name() {
        return "ChromeStreamer";
    }

    @Override
    public void shutdown() {
        appstream.stop();
        return;
    }
    
}
