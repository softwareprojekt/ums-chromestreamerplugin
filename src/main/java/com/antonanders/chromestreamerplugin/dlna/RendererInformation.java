/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.dlna;

import com.google.gson.JsonObject;

/**
 *
 * @author anton
 */
public class RendererInformation {
    public String UUID;
    public String name;
    public String vendor;
    public String icon;
    public String ip;
    
    
    public JsonObject getAsJsonObject(){
        JsonObject obj = new JsonObject();
        obj.addProperty("UUID", UUID);
        obj.addProperty("name", name);
        obj.addProperty("ip", ip);
        obj.addProperty("vendor", vendor);
        obj.addProperty("icon", icon);
        
        return obj;
    }
}
