/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.dlna;

import com.antonanders.chromestreamerplugin.screenrecorder.ScreenStream;
import java.net.InetAddress;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import net.pms.PMS;
import net.pms.configuration.RendererConfiguration;
import net.pms.dlna.DLNAResource;
import net.pms.network.UPNPControl;
import net.pms.network.UPNPHelper;
import net.pms.network.UPNPHelper.Player;

/**
 *
 * @author anton
 */
public class DLNAController {
        
    private RendererConfiguration currentRendererConfiguration;
    private Player player;
    
    protected static DLNAController instance;
    
    public static DLNAController getInstance(){
        if(instance == null){
            instance = new DLNAController();
        }
        return instance;
    }
    
    public boolean setCurrentRenderer(String uuid){
        List<RendererConfiguration> ren = UPNPHelper.getRenderers(UPNPControl.ANY);
        for(RendererConfiguration r : ren){
            if(r.getUUID().equals(uuid)){
                setCurrentRenderer(r);
                return true;
            }
        }
        return false;       
    }
    
    public boolean setCurrentRenderer(InetAddress adress){
        
        List<RendererConfiguration> ren = UPNPHelper.getRenderers(UPNPControl.ANY);
        for(RendererConfiguration r : ren){
            if(r.getAddress().equals(adress)){
                setCurrentRenderer(r);
                return true;
            }
        }
        return false;       
    }
    
    
    public boolean setCurrentRenderer(RendererConfiguration rendererconfiguration){
        
        if(rendererconfiguration != null){
            this.currentRendererConfiguration = rendererconfiguration;
            this.player = rendererconfiguration.getPlayer();//new Player(currentRendererConfiguration);
            System.out.println("Renderer set:"+rendererconfiguration.getUUID());
            System.out.println("Player State:"+player.getState());
            return true;
        }
        
        return false;       
    }
    
    /**
     * Return a list of all availible Renderers matching our requirements
     * 
     * @return List<RendererInformation>
     */
    public List<RendererInformation> getAvailibleRenderer(){
        List<RendererInformation> renderer = new LinkedList<>();
        
        List<RendererConfiguration> ren = UPNPHelper.getRenderers(UPNPControl.AVT);
        for(RendererConfiguration r : ren){
            RendererInformation rend = new RendererInformation();
            rend.UUID = r.getUUID();
            rend.name = r.getRendererName();
            rend.icon = r.getRendererIcon();
            renderer.add(rend);
        }
        System.out.println("INFO added some renderer "+renderer.size());
        return renderer;
    }
    
    
    public RendererInformation getRendererByUUID(String uuid){
        List<RendererConfiguration> ren = UPNPHelper.getRenderers(UPNPControl.AVT);
        for(RendererConfiguration r : ren){
            if(!r.getUUID().equals(uuid))
                continue;
            
            RendererInformation rend = new RendererInformation();
            rend.UUID = r.getUUID();
            rend.name = r.getRendererName();
            rend.icon = r.getRendererIcon();
            return rend;
        }
        return new RendererInformation();
    }
    
    public RendererInformation getCurrentRenderer(){
        return this.getRendererByUUID( this.player.renderer.getUUID() );
    }
    
    public boolean play(DLNAResource resource){
        
        return this.play();
        /*
        if(     currentRendererConfiguration instanceof RendererConfiguration 
            &&  currentRendererConfiguration.isUpnpControllable() 
            && !currentRendererConfiguration.isOffline())
        {
            System.out.println("INFO play ressource");
            //UPNPHelper.play(resource, currentRendererConfiguration);
            //player.setURI(resource.getURL(""),null); //, resource.getDidlString(currentRendererConfiguration)
            System.out.println("Playlist-Size:"+this.player.getPlaylist().getSize());
            System.out.println("Renderer-UUID:"+this.player.renderer.uuid);
            System.out.println(this.player.getState());
            //player.play();
            player.pressPlay(resource.getURL(""), null);
                
            
            
            
            System.out.println("URL:"+resource.getURL(""));
            System.out.println("Playlist-Size:"+this.player.getPlaylist().getSize());
            System.out.println("Renderer-UUID:"+this.player.renderer.uuid);
            return true;
        }
        
        return false;*/
    }
    
    public boolean play(){
        if(     currentRendererConfiguration instanceof RendererConfiguration 
            &&  currentRendererConfiguration.isUpnpControllable() 
            && !currentRendererConfiguration.isOffline())
        {
            System.out.println("INFO play ressource");
            //UPNPHelper.play(resource, currentRendererConfiguration);
            //player.setURI(resource.getURL(""),null); //, resource.getDidlString(currentRendererConfiguration)
            System.out.println("Playlist-Size:"+this.player.getPlaylist().getSize());
            System.out.println("Renderer-UUID:"+this.player.renderer.uuid);
            System.out.println(this.player.getState());
            //player.play();
            //player.pressPlay(resource.getURL(""), null);
            PMS.get().getRootFolder(currentRendererConfiguration).discoverChildren();
            currentRendererConfiguration.getRootFolder().discoverChildren();
            for(DLNAResource r : currentRendererConfiguration.getRootFolder().getChildren()){
                System.out.println(r.getName()+ " " + r.getClass().toString());
                System.out.println(r.getName());
                if(r instanceof ScreenStream){
                    player.pressPlay(r.getURL(""), null);
                    System.out.println("URL:"+r.getURL(""));
                    System.out.println("Playlist-Size:"+this.player.getPlaylist().getSize());
                    System.out.println("Renderer-UUID:"+this.player.renderer.uuid);
                }
            }
            
            
            
            
            return true;
        }
        
        return false;
    }
    
    public void stop(){
        player.stop();
    }
    
    public void setVolume(int volume){
        player.setVolume(volume);
    }
}
