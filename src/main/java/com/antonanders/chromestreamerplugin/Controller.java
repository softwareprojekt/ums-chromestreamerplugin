/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin;

import com.antonanders.chromestreamerplugin.Server.AppServer;
import com.antonanders.chromestreamerplugin.Server.ChromeServer;
import com.antonanders.chromestreamerplugin.dlna.DLNAController;
import com.antonanders.chromestreamerplugin.screenrecorder.ScreenStream;
import com.antonanders.chromestreamerplugin.totv.Screencast;
import net.pms.configuration.RendererConfiguration;
import net.pms.dlna.DLNAResource;
import net.pms.dlna.WebStream;
import net.pms.dlna.WebVideoStream;
import net.pms.network.UPNPControl;
import net.pms.network.UPNPHelper;

/**
 *
 * @author anton
 */
public class Controller {
    private AppServer               app;

    //private KurentoMediaPipeline    kurento;
    private DLNAController          dlna;
    
    private String                  urlToStream;
    
    /**
     * Standart port for incoming calls from the App
     */
    public static final int HTTP_PORT = 10980;
    
    /**
     * Standart port for the connection to our chrome extension
     */
    public static final int WEBSOCKET_PORT = 10981;
    
    /**
     * Standart port for the connection to our chrome extension
     */
    public static final String STREAM_NAME = "ChromeStream";
    
    /**
     * We initialize all Servers, Create the Media Pipeline and set everything
     * to wait for incoming calls
     */
    public Controller() {
        init();
    }

    private void init(){
        ChromeServer.getInstance();
        AppServer.getInstance();
    }
    
    /**
     * This function returns a DLNAResource, mainly WebVideoStream
     * This method can be called from outside becourse we want to allow external DMC to load our stream
     * ChromeStreamerPlugin can call this function when answering to an DMC in getChild()
     * 
     * @return DLNAResource
     */
    public DLNAResource getStream(){
        /*System.out.println("INFO getScreen()");
        Screencast stream = new Screencast();
        stream.init();*/
        //return stream;
        return new ScreenStream();
    } 
    
    /**
     * Send the current Stream to the selected Renderer
     */
    private void sendStreamToDevice(){
        DLNAController.getInstance().play(getStream());
    }
    
}
