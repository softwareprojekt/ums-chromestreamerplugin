/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server;

//import com.antonanders.chromestreamerplugin.kurento.KurentoMediaPipeline;
import com.antonanders.chromestreamerplugin.Server.ChromeExt.ResponseEventInterface;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import java.util.Map;
import org.webbitserver.WebSocketConnection;
import org.webbitserver.WebSocketHandler;

/**
 *
 * @author anton
 */
public class ChromeWebsocketHandler implements WebSocketHandler{

    protected static ChromeWebsocketHandler instance;
    protected WebSocketConnection connection;
    
    protected Map<String, ResponseEventInterface> responsecallback;
    
    
    public static ChromeWebsocketHandler getInstance(){
        if(ChromeWebsocketHandler.instance ==null){
            ChromeWebsocketHandler.instance = new ChromeWebsocketHandler();
        }
        return ChromeWebsocketHandler.instance;
    }
    
    
    public static void registerCallback(String key, ResponseEventInterface respObj){
        ChromeWebsocketHandler.getInstance().responsecallback.put(key, respObj);
    }
    
    public static void send(JsonObject data){
        System.out.println("DATA2CHROME: "+data.toString());
        if(ChromeWebsocketHandler.getInstance().connection != null)
            ChromeWebsocketHandler.getInstance().connection.send(data.toString());
    }
    
    public static boolean send(String data){
        System.out.println("DATA2CHROME: "+data);
        if(ChromeWebsocketHandler.getInstance().connection != null){
            ChromeWebsocketHandler.getInstance().connection.send(data);
            return true;
        }
        return false;
    }
    
    public void send(WebSocketConnection wsc, JsonObject data){
        wsc.send(data.toString());
    }

    @Override
    public void onOpen(WebSocketConnection wsc) throws Throwable {
        connection = wsc;
    }

    @Override
    public void onClose(WebSocketConnection wsc) throws Throwable {
        connection = null;
    }

    @Override
    public void onMessage(WebSocketConnection wsc, String string) throws Throwable {
        System.out.println(string);
        JsonObject data = (JsonObject)new JsonParser().parse(string);
        
        for( String key : this.responsecallback.keySet()){
            if(key.equals( data.get("function").getAsString() )){
                this.responsecallback.get(key).processResponse( data );
            }
        }
    }

    @Override
    public void onMessage(WebSocketConnection wsc, byte[] bytes) throws Throwable {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onPing(WebSocketConnection wsc, byte[] bytes) throws Throwable {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onPong(WebSocketConnection wsc, byte[] bytes) throws Throwable {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
