/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.AppHandles;

import com.antonanders.chromestreamerplugin.Server.AppHandles.AbstractAppHandler;
import com.antonanders.chromestreamerplugin.Server.ChromeExt.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author anton
 */
public class SendURLHandler extends AbstractAppHandler {

    public SendURLHandler() {
    }

    @Override
    protected String processRequest(HttpExchange he) {
        Map<String, String> params = this.getParams(he);
        String url      = this.getParamValue(params, "url");
        String cookie   = this.getParamValue(params, "cookie");
        
        boolean response = false;
        
        
        SendURLCall click = new SendURLCall();
        
        click.setURL(url);
        click.setCookie(cookie);
        
        response = click.sendToChrome();
        
        return "{response:"+response+"}";
    }


    
}
