/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.AppHandles;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.OutputStream;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author anton
 */
public abstract class AbstractAppHandler  implements HttpHandler{

    @Override
    public void handle(HttpExchange he) throws IOException {
        System.out.println("INFO: Handle "+he.getRequestURI());
        he.getResponseHeaders().add("Content-type", "application/json");
        
        String response = processRequest(he);
        
        he.sendResponseHeaders( 200, response.length() );

        System.out.println("INFO: Response "+response);
        try (OutputStream os = he.getResponseBody()) {
            os.write( response.getBytes() );
        }
    }
    
    protected Map<String, String> getParams(HttpExchange he){
        Map<String, String> params = new HashMap<>();
        String[] rawparams = he.getRequestURI().getQuery().split("&");
        for(String par: rawparams){
            String[] param_splitted = par.split("=");
            if(param_splitted.length > 1)
                params.put(param_splitted[0].toLowerCase(), param_splitted[1].toLowerCase());
            else
                params.put(param_splitted[0].toLowerCase(),"");
        }
        
        return params;
    }
    
    protected String getParamValue(Map<String, String> parammap, String key){
        String ret = parammap.get(key);
        return (ret == null)? "" : ret;
    }
    
    /**
     * Process the Request and generate a ResponseString
     * We want to send JSON-Data by default
     * @param he
     * @return String
     */
    protected abstract String processRequest(HttpExchange he);
    
}
