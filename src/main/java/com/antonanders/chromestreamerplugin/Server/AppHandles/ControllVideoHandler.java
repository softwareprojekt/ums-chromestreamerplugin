/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.AppHandles;

import com.antonanders.chromestreamerplugin.Server.ChromeExt.*;
import com.sun.net.httpserver.HttpExchange;
import java.util.Map;

/**
 *
 * @author anton
 */
public class ControllVideoHandler extends AbstractAppHandler {

    public ControllVideoHandler() {
    }

    @Override
    protected String processRequest(HttpExchange he) {
        Map<String, String> params = this.getParams(he);
        String key     = this.getParamValue(params, "key");
        
        boolean response = false;
        
        VideoControllCall click = new VideoControllCall();
        
        switch(key){
            case "play": 
                click.setKey(VideoControllCall.PLAY);break;
            case "pause": 
                click.setKey(VideoControllCall.PAUSE);break;
            case "stop": 
                click.setKey(VideoControllCall.STOP);break;
            case "fullscreen": 
                click.setKey(VideoControllCall.FULLSCREEN);break;
        }
        
        
        response = click.sendToChrome();
        
        return "{response:"+response+"}";
    }


    
}
