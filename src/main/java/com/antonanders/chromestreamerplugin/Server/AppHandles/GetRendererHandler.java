package com.antonanders.chromestreamerplugin.Server.AppHandles;

import com.antonanders.chromestreamerplugin.dlna.DLNAController;
import com.antonanders.chromestreamerplugin.dlna.RendererInformation;
import com.google.gson.JsonArray;
import com.sun.net.httpserver.HttpExchange;
import java.util.List;


/**
 *
 * @author anton
 */
public class GetRendererHandler extends AbstractAppHandler {

    public GetRendererHandler() {
    }

    @Override
    protected String processRequest(HttpExchange he) {
        System.out.println("INFO processRequest /getRenderer");
        JsonArray response = new JsonArray();
        List<RendererInformation> infos = DLNAController.getInstance().getAvailibleRenderer();
        if(!infos.isEmpty()){
            for(RendererInformation item : infos){
                response.add(item.getAsJsonObject());
                
            }
        }
       
        return response.toString();
    }

    
    
}
