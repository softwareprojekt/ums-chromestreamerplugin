/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.AppHandles;

import com.antonanders.chromestreamerplugin.Server.AppHandles.AbstractAppHandler;
import com.antonanders.chromestreamerplugin.Server.ChromeExt.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anton
 */
public class PageBackHandler extends AbstractAppHandler {

    public PageBackHandler() {
    }

    @Override
    protected String processRequest(HttpExchange he) {

        boolean response = false;
        
        //PageBackCall click = new PageBackCall();
        
        Robot rob;
        try {
            rob = new Robot();
            rob.keyPress(KeyEvent.VK_ALT);
            rob.keyPress(KeyEvent.VK_LEFT);
            rob.keyRelease(KeyEvent.VK_ALT);
            rob.keyRelease(KeyEvent.VK_LEFT);
            System.out.println("BACK");
            response = true;
        } catch (AWTException ex) {
            Logger.getLogger(PageForwardHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //response = click.sendToChrome();
        
        return "{response:"+response+"}";
    }


    
}
