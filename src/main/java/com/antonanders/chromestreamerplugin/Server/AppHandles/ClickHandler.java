/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.AppHandles;

import com.antonanders.chromestreamerplugin.Server.AppHandles.AbstractAppHandler;
import com.antonanders.chromestreamerplugin.Server.ChromeExt.AbstractExtensionCall;
import com.antonanders.chromestreamerplugin.Server.ChromeExt.ClickCall;
import com.antonanders.chromestreamerplugin.dlna.DLNAController;
import com.antonanders.chromestreamerplugin.screenrecorder.ScreenStream;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author anton
 */
public class ClickHandler extends AbstractAppHandler {

    public ClickHandler() {
    }


    @Override
    protected String processRequest(HttpExchange he) {
        Map<String, String> params = this.getParams(he);
        String key      = this.getParamValue(params, "key");
        String x        = this.getParamValue(params, "x");
        String y        = this.getParamValue(params, "y");
        
        boolean response = false;
        
        
        ClickCall click = new ClickCall();
        
        click.setX(x);
        click.setY(y);
        
        if(key.contains("left")){
            click.setKey(ClickCall.MOUSE_LEFT);
        }else
        if(key.contains("right")){
            click.setKey(ClickCall.MOUSE_RIGHT);
        }else
        if(key.contains("mid")){
            click.setKey(ClickCall.MOUSE_MID);
        }
        
        response = click.perform();
        
        
        return "{response:"+response+"}";
    }
    
}
