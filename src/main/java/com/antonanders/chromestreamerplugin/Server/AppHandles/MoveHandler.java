/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.AppHandles;

import com.antonanders.chromestreamerplugin.Server.AppHandles.AbstractAppHandler;
import com.antonanders.chromestreamerplugin.Server.ChromeExt.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author anton
 */
public class MoveHandler extends AbstractAppHandler {

    public MoveHandler() {
    }

    @Override
    protected String processRequest(HttpExchange he) {
        Map<String, String> params = this.getParams(he);
        String x        = this.getParamValue(params, "x");
        String y        = this.getParamValue(params, "y");
        
        boolean response = false;
        
        
        MoveCall click = new MoveCall();
        
        click.setX(x);
        click.setY(y);
        
        response = click.perform();
        
        return "{response:"+response+"}";
    }

    
}
