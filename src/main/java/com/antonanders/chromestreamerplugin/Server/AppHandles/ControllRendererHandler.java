/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.AppHandles;

import com.antonanders.chromestreamerplugin.Server.AppHandles.AbstractAppHandler;
import com.antonanders.chromestreamerplugin.dlna.DLNAController;
import com.antonanders.chromestreamerplugin.screenrecorder.ScreenStream;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author anton
 */
public class ControllRendererHandler extends AbstractAppHandler {

    public ControllRendererHandler() {
    }

    @Override
    protected String processRequest(HttpExchange he) {
        Map<String, String> params = this.getParams(he);
        String key      = this.getParamValue(params, "key");
        String value    = this.getParamValue(params, "value");
        
        boolean response = false;
        
        if(key.equalsIgnoreCase( "volume" )){
            DLNAController.getInstance().setVolume(new Integer(value));
        }
        if(key.equalsIgnoreCase("key")){
            switch(value){
                case "play":
                    response = DLNAController.getInstance().play(new ScreenStream());
                    break;
                case "stop":
                    DLNAController.getInstance().stop();
                    break;
            }
        }
        return "{response:"+response+"}";
    }

    
}
