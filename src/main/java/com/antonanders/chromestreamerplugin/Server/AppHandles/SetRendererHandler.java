/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.AppHandles;

import com.antonanders.chromestreamerplugin.dlna.DLNAController;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;


/**
 *
 * @author anton
 */
public class SetRendererHandler extends AbstractAppHandler {

    public SetRendererHandler() {
    }


    @Override
    protected String processRequest(HttpExchange he) {
        String uuid = this.getParamValue(
                this.getParams(he), 
                "uuid"
        );
        
        System.out.println("UUID:"+uuid);
        
        boolean set = DLNAController.getInstance().setCurrentRenderer(uuid);
        
        JsonObject json = new JsonObject();
        json.addProperty("setRenderer", set);
        return json.toString();
    
    }
    
}
