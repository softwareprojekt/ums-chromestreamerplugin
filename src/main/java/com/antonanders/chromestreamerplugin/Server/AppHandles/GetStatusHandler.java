/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.AppHandles;

import com.antonanders.chromestreamerplugin.Server.AppHandles.AbstractAppHandler;
import com.antonanders.chromestreamerplugin.Server.ChromeExt.*;
import com.antonanders.chromestreamerplugin.dlna.DLNAController;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author anton
 */
public class GetStatusHandler extends AbstractAppHandler {

    public GetStatusHandler() {
    }

    @Override
    protected String processRequest(HttpExchange he) {

        boolean response = false;
        
        
        GetInformationCall info = new GetInformationCall();
        
        info.registerCallback();
        
        JsonObject data = new JsonObject();
        
        response = info.sendToChromeSynchronious();
        data.add("target", DLNAController.getInstance().getCurrentRenderer().getAsJsonObject());
        
        data.addProperty("status", info.getStatus());
        
        return data.getAsString();
    }
    
}
