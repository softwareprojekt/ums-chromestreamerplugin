/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.AppHandles;

import com.antonanders.chromestreamerplugin.Server.AppHandles.AbstractAppHandler;
import com.antonanders.chromestreamerplugin.Server.ChromeExt.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author anton
 */
public class ScrollHandler extends AbstractAppHandler {

    public ScrollHandler() {
    }

    @Override
    protected String processRequest(HttpExchange he) {
        Map<String, String> params = this.getParams(he);
        String dx        = this.getParamValue(params, "dx");
        String dy        = this.getParamValue(params, "dy");
        
        boolean response = false;
        
        
        ScrollCall click = new ScrollCall();
        
        click.setDX(dx);
        click.setDY(dy);
        
        
        response = click.sendToChrome();
        
        return "{response:"+response+"}";
    }

    
}
