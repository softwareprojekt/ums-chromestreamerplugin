/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server;


import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.antonanders.chromestreamerplugin.Server.AppHandles.*;
/**
 *
 * @author anton
 */
public class AppServer {
    HttpServer server;

    protected boolean running = false;
    
    protected static AppServer instance;
    
    public static AppServer getInstance(){
        if(instance == null){
            instance = new AppServer();
        }
        return instance;
    }
    
    public AppServer() {
        System.out.println("INFO: Init App-HTTP-Server on Port 10980");
        try {
            server = HttpServer.create(new InetSocketAddress(10980), 0);
            server.createContext("/click",              new ClickHandler());
            server.createContext("/move",               new MoveHandler());
            server.createContext("/scroll",             new ScrollHandler());
            server.createContext("/zoom",               new ZoomHandler());
            server.createContext("/getRenderer",        new GetRendererHandler());
            server.createContext("/setRenderer",        new SetRendererHandler());
            server.createContext("/controllVideo",      new ControllVideoHandler());
            server.createContext("/controllRenderer",   new ControllRendererHandler());
            server.createContext("/getStatus",          new GetStatusHandler());
            server.createContext("/sendURL",            new SendURLHandler());
            server.createContext("/sendText",           new SendTextHandler());
            server.createContext("/pageForward",        new PageForwardHandler());
            server.createContext("/pageBack",           new PageBackHandler());
            server.createContext("/refreshPage",        new RefreshPageHandler());
            
            server.start();
            
            running = true;
            System.out.println("INFO: App-HTTP-Server started");
        } catch (IOException ex) {
            Logger.getLogger(AppServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void stop(){
        server.stop(0);
        running = false;
    }
    
    public boolean isRunning(){
        return running;
    }
    
    
    
    
    
    
}
