/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server;

import org.webbitserver.WebServer;
import org.webbitserver.WebServers;


/**
 *
 * @author anton
 */

public class ChromeServer {

    protected static WebServer server;
    
    public static WebServer getInstance() {
        if(ChromeServer.server == null){
            ChromeServer.server = initWebserver();
        }
        return ChromeServer.server;
    }
    
    private static WebServer initWebserver(){
        WebServer s =  WebServers.createWebServer(10981);
        s.add("/dlnaserver", ChromeWebsocketHandler.getInstance());
        s.start();
        
        return s;
    }
    
    
    /*
    Sender for chromeExt
    */
    
    
}
