/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.ChromeExt;

import com.antonanders.chromestreamerplugin.Server.ChromeWebsocketHandler;
import com.google.gson.JsonObject;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anton
 */
public abstract class AbstractExtensionCall implements ResponseEventInterface{
    
    protected boolean got_response = false;
    
    public static final long MAX_TIMEOUT = 100000;
    
    abstract protected String getFunctionName();
    
    abstract protected JsonObject addDataToJsonObject(JsonObject data);
    
    @Override
    public String toString(){
        JsonObject data = new JsonObject();
        
        data.addProperty("function", this.getFunctionName() );
        data = this.addDataToJsonObject(data);
        
        return data.toString();
    }
    
    public boolean sendToChrome(){
        this.got_response = false;
        return ChromeWebsocketHandler.send(this.toString());
    }
    
    public boolean sendToChromeSynchronious(){
        
        this.sendToChrome();
        
        try {
            this.wait(AbstractExtensionCall.MAX_TIMEOUT);
        } catch (InterruptedException ex) {
            
        }
        
        return this.got_response;
    }
    
    @Override
    public void processResponse(JsonObject data){
        this.got_response = true;
        this.notify();
    }
    
    public void registerCallback(){
        ChromeWebsocketHandler.registerCallback(this.getFunctionName(), this);
    }
}
