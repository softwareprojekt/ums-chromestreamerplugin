/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.ChromeExt;

import com.google.gson.JsonObject;

/**
 *
 * @author anton
 */
public class VideoControllCall extends AbstractExtensionCall{

    protected String key    = VideoControllCall.PLAY;
            
    public final static String PLAY         = "play";
    public final static String PAUSE        = "pause";
    public final static String STOP         = "stop";
    public final static String FULLSCREEN   = "fullscreen";
    
    
    public void setKey(String key){
        this.key = key;
    }
    
    
    @Override
    protected String getFunctionName() {
        return "controllVideo";
    }

    @Override
    protected JsonObject addDataToJsonObject(JsonObject data) {

        data.addProperty("key", key);
        
        return data;
    }

    
    @Override
    public void processResponse(JsonObject data) {
        super.processResponse(data);
    }
    
}
