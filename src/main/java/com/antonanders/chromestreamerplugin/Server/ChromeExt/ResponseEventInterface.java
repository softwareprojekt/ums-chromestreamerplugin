/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.ChromeExt;

import com.google.gson.JsonObject;

/**
 *
 * @author anton
 */
public interface ResponseEventInterface {
    
    public void processResponse(JsonObject str);
    
}
