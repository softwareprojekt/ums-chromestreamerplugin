/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.ChromeExt;

import com.google.gson.JsonObject;

/**
 *
 * @author anton
 */
public class SendTextCall extends AbstractExtensionCall{

    protected String text         = "";
    
    
    public void setText(String text){
        this.text = text;
    }
    
    
    @Override
    protected String getFunctionName() {
        return "sendText";
    }

    @Override
    protected JsonObject addDataToJsonObject(JsonObject data) {
        
        data.addProperty("text",     text);
        
        return data;
    }

    
    @Override
    public void processResponse(JsonObject data) {
        super.processResponse(data);
    }
    
}
