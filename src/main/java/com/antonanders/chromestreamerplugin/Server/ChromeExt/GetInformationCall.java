/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.ChromeExt;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 *
 * @author anton
 */
public class GetInformationCall extends AbstractExtensionCall{
    
    protected boolean playable_video    = false;
    protected String  status            = "";
    protected boolean loaded            = false;
    
    public static final String PLAY     = "play";
    public static final String PAUSE    = "pause";
    public static final String STOP     = "stop";
    public static final String NONE     = "none";
    
    
    @Override
    protected String getFunctionName() {
        return "getInformation";
    }

    @Override
    protected JsonObject addDataToJsonObject(JsonObject data) {
        return data;
    }
    
    public String getStatus(){
        switch(this.status){
            case GetInformationCall.PLAY    : return "video_playing"; 
            case GetInformationCall.PAUSE   : return "video_paused"; 
            case GetInformationCall.STOP    : return "video_stopped"; 
        }
        
        if(this.playable_video)
            return "idle";
        
        if(this.loaded)
            return "interacting";
        
        return "notloaded";
    }

    @Override
    public void processResponse(JsonObject data) {
        
       // JsonObject data = (JsonObject)new JsonParser().parse(str);
        this.playable_video = data.get("playable_video").getAsBoolean();
        
        switch(data.get("status").getAsString()){
            case "play":    this.status = GetInformationCall.PLAY; break;
            case "pause":   this.status = GetInformationCall.PAUSE; break;
            case "stop":    this.status = GetInformationCall.STOP; break;
        }

        this.loaded         = data.get("loaded").getAsBoolean();
        
        super.processResponse(data);
    }

}
