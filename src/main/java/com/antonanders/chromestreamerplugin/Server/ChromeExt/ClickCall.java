/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.ChromeExt;

import com.google.gson.JsonObject;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anton
 */
public class ClickCall{

    protected int x         = 0;
    protected int y         = 0;
    protected String key    = ClickCall.MOUSE_LEFT;
            
    public final static String MOUSE_LEFT  = "left";
    public final static String MOUSE_RIGHT = "right";
    public final static String MOUSE_MID   = "mid";
    
    
    public final int SIZE_X = 1920; 
    public final int SIZE_Y = 1080; 
    public final int RANGE_X = 1000; 
    public final int RANGE_Y = 1000; 
    
    public void setX(String x){
        this.x = new Integer(x);
        this.x = Integer.min( Integer.max(0, this.x), RANGE_X) ;
        this.x = (SIZE_X * this.x / RANGE_X);
    }
    
    public void setY(String y){
        this.y = new Integer(y);
        this.y = Integer.min( Integer.max(0, this.y), RANGE_Y) ;
        this.y = (SIZE_Y * this.y / RANGE_Y);
    }
    
    public void setKey(String key){
        this.key = key;
    }
    
    
    public boolean perform(){
        try {
            Robot bot = new Robot();
            bot.mouseMove(x, y);
            int mask = 0;
            if(key.equals(ClickCall.MOUSE_LEFT)){
                mask = InputEvent.BUTTON1_DOWN_MASK;
            }
            if(key.equals( ClickCall.MOUSE_RIGHT )){
                mask = InputEvent.BUTTON3_DOWN_MASK;
            }
            if(key.equals( ClickCall.MOUSE_MID) ){
                mask = InputEvent.BUTTON2_DOWN_MASK;
            }
            
            
            bot.mousePress(mask);
            bot.mouseRelease(mask);
        } catch (AWTException ex) {
            Logger.getLogger(ClickCall.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
    
}
