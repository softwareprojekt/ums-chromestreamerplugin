/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.ChromeExt;

import com.google.gson.JsonObject;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anton
 */
public class MoveCall{

    protected int x         = 0;
    protected int y         = 0;
    
    public final int SIZE_X = 1920; 
    public final int SIZE_Y = 1080; 
    public final int RANGE_X = 1000; 
    public final int RANGE_Y = 1000; 
    
    public void setX(String x){
        this.x = new Integer(x);
        this.x = Integer.min( Integer.max(0, this.x), RANGE_X) ;
        this.x = (SIZE_X * this.x / RANGE_X);
    }
    
    public void setY(String y){
        this.y = new Integer(y);
        this.y = Integer.min( Integer.max(0, this.y), RANGE_Y) ;
        this.y = (SIZE_Y * this.y / RANGE_Y);
    }
    
    public boolean perform(){
        try {
            Robot bot = new Robot();
            bot.mouseMove(x, y);
            
        } catch (AWTException ex) {
            Logger.getLogger(ClickCall.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }
    
}
