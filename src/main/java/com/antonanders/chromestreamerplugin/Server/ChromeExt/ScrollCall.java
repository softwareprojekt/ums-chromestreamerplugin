/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.ChromeExt;

import com.google.gson.JsonObject;

/**
 *
 * @author anton
 */
public class ScrollCall extends AbstractExtensionCall{

    protected int dx         = 0;
    protected int dy         = 0;
    
    
    public void setDX(String dx){
        this.dx = new Integer(dx);
    }
    
    public void setDY(String dy){
        this.dy = new Integer(dy);
    }
    
    
    @Override
    protected String getFunctionName() {
        return "scroll";
    }

    @Override
    protected JsonObject addDataToJsonObject(JsonObject data) {
        
        data.addProperty("dx", dx);
        data.addProperty("dy", dy);
        
        return data;
    }

    
    @Override
    public void processResponse(JsonObject data) {
        super.processResponse(data);
    }
    
}
