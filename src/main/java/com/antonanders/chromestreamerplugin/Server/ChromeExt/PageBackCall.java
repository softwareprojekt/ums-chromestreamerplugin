/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.ChromeExt;

import com.google.gson.JsonObject;

/**
 *
 * @author anton
 */
public class PageBackCall extends AbstractExtensionCall{

    
    @Override
    protected String getFunctionName() {
        return "pageBack";
    }

    @Override
    protected JsonObject addDataToJsonObject(JsonObject data) {
        
        return data;
    }

    @Override
    public void processResponse(JsonObject data) {
        super.processResponse(data);
    }
    
}
