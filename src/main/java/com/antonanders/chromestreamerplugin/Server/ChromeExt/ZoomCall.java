/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.ChromeExt;

import com.google.gson.JsonObject;

/**
 *
 * @author anton
 */
public class ZoomCall extends AbstractExtensionCall{

    protected int factor = 0;
    
    
    public void setFactor(String factor){
        this.factor = new Integer(factor);
    }
    
    
    @Override
    protected String getFunctionName() {
        return "zoom";
    }

    @Override
    protected JsonObject addDataToJsonObject(JsonObject data) {
        
        data.addProperty("factor", factor);
        
        return data;
    }
    
    @Override
    public void processResponse(JsonObject data) {
        super.processResponse(data);
    }
    
}
