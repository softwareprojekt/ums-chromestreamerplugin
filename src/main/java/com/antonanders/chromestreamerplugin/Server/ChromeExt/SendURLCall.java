/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.Server.ChromeExt;

import com.google.gson.JsonObject;

/**
 *
 * @author anton
 */
public class SendURLCall extends AbstractExtensionCall{

    protected String url         = "";
    protected String cookie      = "";
    

    
    
    public void setURL(String url){
        this.url = url;
    }
    
    public void setCookie(String cookie){
        this.cookie = cookie;
    }
    
    
    @Override
    protected String getFunctionName() {
        return "sendURL";
    }

    @Override
    protected JsonObject addDataToJsonObject(JsonObject data) {
        
        data.addProperty("url",     url);
        data.addProperty("cookie",  cookie);

        
        return data;
    }

    
    @Override
    public void processResponse(JsonObject data) {
        super.processResponse(data);
    }
    
}
