/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.screenrecorder;

import com.sun.jna.Platform;
import java.util.ArrayList;
import java.util.List;
import net.pms.PMS;
import net.pms.io.OutputParams;
import net.pms.io.PipeProcess;
import net.pms.io.ProcessWrapper;
import net.pms.io.ProcessWrapperImpl;

/**
 *
 * @author anton
 */
public class FFMPEGScreenVideo {
    
    private ProcessWrapperImpl pw;
    
    private  String RTSP_ADDRESS = "127.0.0.1"; 
    private  Integer RTSP_PORT   = 8090; 
    private  String RTSP_LOCATOR = "screen.ffm"; 
    
    public FFMPEGScreenVideo(){
        
        
    }
    
    public void setRTSPServerAdress(String add){
        this.RTSP_ADDRESS = add;
    }
    
    public void setRTSPServerPort(Integer port){
        this.RTSP_PORT = port;
    }
    
    public void setRTSPServerLocator(String locator){
        this.RTSP_LOCATOR = locator;
    }
    
    public String executable() {
	return PMS.getConfiguration().getFfmpegPath()+"2";
    }
    
    
    public void start(){
        List<String> cmdList = new ArrayList<>();

        int nThreads = 0;


        cmdList.add(executable());


        // Prevent FFmpeg timeout
        cmdList.add("-y");

        cmdList.add("-loglevel");

        cmdList.add("debug");

        //cmdList.add("debug");
        cmdList.add("-s");
        cmdList.add("1920x1080");

        cmdList.add("-framerate");
        cmdList.add("25");

        cmdList.add("-f");
        if (Platform.isWindows()) {
                cmdList.add("dshow");
        } else {
                cmdList.add("x11grab");
        }

        // Decoder threads
        if (nThreads > 0) {
                cmdList.add("-threads");
                cmdList.add(String.valueOf(nThreads));
        }

        cmdList.add("-i");
        if (Platform.isWindows()) {
                cmdList.add("video=\"UScreenCapture\":audio=\"Microphone\"");
        } else {
                cmdList.add(":0.0");
        }

        // Encoder threads
        if (nThreads > 0) {
                cmdList.add("-threads");
                cmdList.add(String.valueOf(nThreads));
        }
        /*
        cmdList.add("-ar");
        cmdList.add("44100");

        if (!Platform.isWindows()) {
                cmdList.add("-f");
                cmdList.add("alsa");

                cmdList.add("-i");
                cmdList.add("pulse_monitor");
        }
        */
        cmdList.add("-profile:v");
        cmdList.add("baseline"); 
        cmdList.add("-level");
        cmdList.add("3.0");

        cmdList.add("-c:v");
        cmdList.add("libx264");
        cmdList.add("-preset");
        cmdList.add("fast");
        //cmdList.add("-level");
        //cmdList.add("21");
        cmdList.add("-pix_fmt");
        cmdList.add("yuv420p");
        
        cmdList.add("-slice-max-size");
        cmdList.add("1500");//PACKET SIZE
        cmdList.add("-maxrate");
        cmdList.add("5000"); //CONNECTION SPEED
        cmdList.add("-bufsize");
        cmdList.add("200"); //Buffer per Frame*/
        cmdList.add("-crf");
        cmdList.add("30"); //Quality 18 - 30, lower is better but higher bitrate
    
        cmdList.add("-intra-refresh");
        cmdList.add("1");
        
        cmdList.add("-tune");
        cmdList.add("zerolatency");
        /*
        cmdList.add("-bufsize");
        cmdList.add("64k");
        cmdList.add("-b:v");
        cmdList.add("64k");
        cmdList.add("-minrate");
        cmdList.add("64k");
        cmdList.add("-maxrate");
        cmdList.add("64k");
*/
        cmdList.add("-vf");
        cmdList.add("scale=800:480");
        
        cmdList.add("-an");

        /*cmdList.add("-c:a");
        cmdList.add("aac");
        cmdList.add("-ab");
        cmdList.add("128k");
        cmdList.add("-ar");
        cmdList.add("44100");
        */
        cmdList.add("-me_range");
        cmdList.add("1");
        
        cmdList.add("-movflags");
        cmdList.add("+faststart");
        
       // cmdList.add("-b:v");
       // cmdList.add("64k");
        
        
        
        cmdList.add("http://"+RTSP_ADDRESS+":"+RTSP_PORT+"/"+RTSP_LOCATOR);

        OutputParams params = new OutputParams(PMS.getConfiguration());

        String[] cmdArray = new String[cmdList.size()];
        cmdList.toArray(cmdArray);

        for(String ar: cmdArray){
            System.out.print(ar+" ");
        }
        System.out.println();
        ProcessWrapperImpl pw_loc = new ProcessWrapperImpl(cmdArray, params);

        try {
                Thread.sleep(50);
        } catch (InterruptedException e) {
        }

        pw_loc.runInNewThread();

        System.out.println("SCREEN: "+pw_loc.toString());

        this.stop();
        

        this.pw = pw_loc;
    }
    
    public void stop(){
        if( pw != null && this.pw.isAlive()){
            this.pw.stopProcess();
        }
    }
    
}
