/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.screenrecorder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author anton
 */
public class PulseAudioMonitor {
    
        public static String getMonitorDevice(){
        String s = null;
        try {
            Process p = Runtime.getRuntime().exec("pactl list sources");
            
            p.waitFor();
 
 //           System.out.printf("PROCESS STARTED");
            
            BufferedReader reader = 
                 new BufferedReader(new InputStreamReader(p.getInputStream()));

//            System.out.printf("BUFFER READ");
            
            String line = "";			
            while ((line = reader.readLine())!= null) {
                //System.out.println(line);
                if(line.contains("analog-stereo.monitor")){
                    String[] st = line.trim().split(" ");
                    if(st.length > 1){
                        s = st[st.length-1];
                        break;
                    }
                }
            }
            
        } catch (IOException ex) {
            Logger.getLogger(PulseAudioMonitor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(PulseAudioMonitor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        return s;
    }
    
    
}
