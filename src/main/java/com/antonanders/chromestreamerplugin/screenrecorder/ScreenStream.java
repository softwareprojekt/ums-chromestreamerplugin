/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.screenrecorder;

import com.antonanders.chromestreamerplugin.dlna.DLNAController;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import net.pms.configuration.FormatConfiguration;
import net.pms.configuration.RendererConfiguration;
import net.pms.dlna.DLNAMediaAudio;
import net.pms.dlna.DLNAMediaInfo;
import net.pms.dlna.Range;
import net.pms.dlna.RealFile;
import net.pms.external.ExternalListener;
import net.pms.formats.Format;
import net.pms.network.UPNPHelper;
import org.apache.commons.configuration.FileConfiguration;

/**
 *
 * @author anton
 */
public class ScreenStream extends RealFile{

    public ScreenStream() {
        
        super(new File("DoesNotExist.mpg"), "Screen");
        System.out.println("Create Screen DLNARessource "+getName() +" " +getDisplayName());
        this.setDefaultRenderer(RendererConfiguration.getDefaultConf());
        
        //this.selfMPEG1();
        this.selfMPEG2();
        //
        //this.selfMPEG4();
        //this.selfH264();
        
        //this.setId("0$1000");
        //this.setMasterParent((ExternalListener) this);
        //this.getConf().setName(getName());
    }

    @Override
    public String getName() {
        return "Screencast";
    }

    
    
    private void selfMPEG1(){
        this.setPlayer(new ScreenRecorderVideoMPEG1());
        
        DLNAMediaInfo fakemedia = new DLNAMediaInfo();
        DLNAMediaAudio audio = new DLNAMediaAudio();
        fakemedia.setCodecV("mpeg1video");
        fakemedia.setContainer("mpegts");
        fakemedia.setDuration(45d*60);
        fakemedia.setWidth(1920);
        fakemedia.setHeight(1080);
        fakemedia.setFrameRate("25");
        //fakemedia.setBitrate(25000000);
        //fakemedia.setSize(Long.MAX_VALUE);
        //fakemedia.isMpegTS()
        fakemedia.getAudioTracksList().add(audio);
        
        audio.setCodecA("mp2");
        //audio.setBitRate(128000);
        audio.setSampleFrequency("44100");
        audio.getAudioProperties().setNumberOfChannels(2);
        
        this.setSkipTranscode(false);
        
        this.setMedia(fakemedia);
        this.setMediaAudio(audio);
    }

    @Override
    public long length() {
        return DLNAMediaInfo.TRANS_SIZE;
    }
    
    
    
    private void selfMPEG2(){
        this.setPlayer(new ScreenRecorderVideoMPEG2());
        
        DLNAMediaInfo fakemedia = new DLNAMediaInfo();
        DLNAMediaAudio audio = new DLNAMediaAudio();
        fakemedia.setCodecV("mpeg2video");
        fakemedia.setContainer("vob");
        fakemedia.setDuration(45d*60);
        fakemedia.setWidth(1920);
        fakemedia.setHeight(1080);
        fakemedia.setFrameRate("25");
        //fakemedia.setMimeType(FormatConfiguration.MPEG2);
        //fakemedia.setSize(Long.MAX_VALUE);
        //fakemedia.setBitrate(25000000);
        //fakemedia.setSize(Long.MAX_VALUE);
        //fakemedia.isMpegTS()
        fakemedia.getAudioTracksList().add(audio);
        
        audio.setCodecA("ac3");
        //audio.setBitRate(128000);
        audio.setSampleFrequency("44100");
        audio.getAudioProperties().setNumberOfChannels(2);
        
        //this.setSkipTranscode(false);
        
        this.setMedia(fakemedia);
        this.setMediaAudio(audio);
    }
    private void selfMPEG4(){
        this.setPlayer(new ScreenRecorderVideoMPEG4());
        
        DLNAMediaInfo fakemedia = new DLNAMediaInfo();
        DLNAMediaAudio audio = new DLNAMediaAudio();
        fakemedia.setCodecV("mpeg4");
        fakemedia.setContainer("avi");
        fakemedia.setDuration(45d*60);
        fakemedia.setWidth(1920);
        fakemedia.setHeight(1080);
        fakemedia.setFrameRate("25");
        //fakemedia.setMimeType(FormatConfiguration.MP4);
        fakemedia.getAudioTracksList().add(audio);
        
        audio.setCodecA("mp3");
        audio.setSampleFrequency("44100");
        audio.getAudioProperties().setNumberOfChannels(2);
        
        
        this.setMedia(fakemedia);
        this.setMediaAudio(audio);
        
    }
    
    
    private void selfH264(){
        this.setPlayer(new ScreenRecorderVideo());
        
        DLNAMediaInfo fakemedia = new DLNAMediaInfo();
        DLNAMediaAudio audio = new DLNAMediaAudio();
        fakemedia.setCodecV("x264");
        fakemedia.setContainer("avi");
        fakemedia.setDuration(45d*60);
        fakemedia.setWidth(1920);
        fakemedia.setHeight(1080);
        fakemedia.setFrameRate("25");
        fakemedia.setMimeType(FormatConfiguration.H264);
        fakemedia.getAudioTracksList().add(audio);
        
        audio.setCodecA("mp3");
        audio.setSampleFrequency("44100");
        audio.getAudioProperties().setNumberOfChannels(2);
        
        
        this.setMedia(fakemedia);
        this.setMediaAudio(audio);
    }

    @Override
    public boolean isValid() {
        return true;
       // return super.isValid(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
