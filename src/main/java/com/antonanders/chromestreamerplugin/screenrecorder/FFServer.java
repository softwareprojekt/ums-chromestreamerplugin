/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.screenrecorder;

import java.util.ArrayList;
import java.util.List;
import net.pms.PMS;
import net.pms.io.OutputParams;
import net.pms.io.ProcessWrapperImpl;

/**
 *
 * @author anton
 */
public class FFServer {
    
    private ProcessWrapperImpl pw;
    
    public boolean isRunning(){
        return this.pw.isAlive();
    }
    
    public String executable() {
	return PMS.getConfiguration().getFfmpegPath().replace("ffmpeg", "ffserver");
    }
    
    public String FFServerConfigPath() {
	return this.executable()+".conf";
    }
    
    public void start(){
        List<String> cmdList = new ArrayList<>();
        
        cmdList.add(this.executable());
        
        cmdList.add("-d");
        cmdList.add("-f");
        cmdList.add(this.FFServerConfigPath());

        OutputParams params = new OutputParams(PMS.getConfiguration());

        String[] cmdArray = new String[cmdList.size()];
        cmdList.toArray(cmdArray);

        for(String ar: cmdArray){
            System.out.print(ar+" ");
        }
        System.out.println();
        ProcessWrapperImpl pw_loc = new ProcessWrapperImpl(cmdArray, params);

        try {
                Thread.sleep(50);
        } catch (InterruptedException e) {
        }

        pw_loc.runInNewThread();

        System.out.println("FFSERVER: "+pw_loc.toString());

        this.stop();
        
        this.pw = pw_loc;
    }
    
    
    
    public void stop(){
        if(pw != null && pw.isAlive()){
            pw.stopProcess();
        }
    }
}
