/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.antonanders.chromestreamerplugin.screenrecorder;

/**
 *
 * @author anton
 */

import com.sun.jna.Platform;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.pms.dlna.DLNAMediaInfo;
import net.pms.dlna.DLNAResource;
import net.pms.encoders.FFMpegVideo;
import net.pms.io.OutputParams;
import net.pms.io.PipeProcess;
import net.pms.io.ProcessWrapper;
import net.pms.io.ProcessWrapperImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScreenRecorderVideoMPEG1 extends FFMpegVideo {
	private static final Logger LOGGER = LoggerFactory.getLogger(ScreenRecorderVideoMPEG1.class);

	public ScreenRecorderVideoMPEG1() {
            
	}
        @Override
    public boolean isTimeSeekable() {
        return false;
    }


	@Override
	public ProcessWrapper launchTranscode(
		DLNAResource dlna,
		DLNAMediaInfo media,
		OutputParams params
	) throws IOException {
		List<String> cmdList = new ArrayList<>();

		/*
		 * FFmpeg uses multithreading by default, so provided that the
		 * user has not disabled FFmpeg multithreading and has not
		 * chosen to use more or less threads than are available, do not
		 * specify how many cores to use.
		 */
		int nThreads = 1;
		if (configuration.isFfmpegMultithreading()) {
			if (Runtime.getRuntime().availableProcessors() == configuration.getNumberOfCpuCores()) {
				nThreads = 0;
			} else {
				nThreads = configuration.getNumberOfCpuCores();
			}
		}

		cmdList.add(executable());
                
                
		// Prevent FFmpeg timeout
		cmdList.add("-y");
/*
		cmdList.add("-loglevel");
		if (LOGGER.isTraceEnabled()) { // Set -loglevel in accordance with LOGGER setting
			cmdList.add("info"); // Could be changed to "verbose" or "debug" if "info" level is not enough
		} else {
			cmdList.add("verbose");
		}*/
                //cmdList.add("debug");
		cmdList.add("-s");
		cmdList.add("1920x1080");

		cmdList.add("-framerate");
		cmdList.add("25");

		cmdList.add("-f");
		if (Platform.isWindows()) {
                    cmdList.add("dshow");
		} else {
                    cmdList.add("x11grab");
		}

		// Decoder threads
		if (nThreads > 0) {
			cmdList.add("-threads");
			cmdList.add(String.valueOf(nThreads));
		}

		cmdList.add("-i");
		if (Platform.isWindows()) {
			cmdList.add("video=\"UScreenCapture\":audio=\"Microphone\"");
		} else {
			cmdList.add(":0.0");
		}

		// Encoder threads
		if (nThreads > 0) {
			cmdList.add("-threads");
			cmdList.add(String.valueOf(nThreads));
		}
                cmdList.add("-ar");
                cmdList.add("44100");
                
                if (!Platform.isWindows()) {
			cmdList.add("-f");
			cmdList.add("pulse");

			cmdList.add("-i");
			cmdList.add(PulseAudioMonitor.getMonitorDevice());
		}
                
		cmdList.add("-c:v");
		cmdList.add("mpeg1video");
		
                //cmdList.add("-q:v");
                //cmdList.add("3");
                //cmdList.add("-g");
                //cmdList.add("15");
                
                cmdList.add("-pix_fmt");
                cmdList.add("yuv420p");
                
                //cmdList.add("-r");
                //cmdList.add("25");
                
                cmdList.add("-b:v");
                cmdList.add("25000k");
                //cmdList.add("-bt");
               // cmdList.add("1000k");
                cmdList.add("-maxrate"); 
                cmdList.add("499000000");
                
         /*     cmdList.add("-g");
                cmdList.add("1");
                cmdList.add("-q:v");
                cmdList.add("1");
                cmdList.add("-qmin"); 
                cmdList.add("2");
                cmdList.add("-qmax");
                cmdList.add("3"); 
                //cmdList.add("-c:a");
                //cmdList.add("copy");
                */

                
               // cmdList.add("-an");
                cmdList.add("-c:a");
                cmdList.add("mp2");
                cmdList.add("-ab");
                cmdList.add("128k");
                cmdList.add("-ar");
                cmdList.add("44100");
                //cmdList.add("-async");
                //cmdList.add("1");
                
                //cmdList.add("-movflags");
                //cmdList.add("+faststart");
                
                cmdList.add("-f");
                cmdList.add("mpeg");
   
                //cmdList.addAll(this.getVideoTranscodeOptions(dlna, media, params));
                //cmdList.addAll(this.getVideoBitrateOptions(dlna, media, params));
                //cmdList.addAll(this.getAudioBitrateOptions(dlna, media, params));
                
		PipeProcess pipe = new PipeProcess("ffmpegscreencastvideo" + System.currentTimeMillis(), params);
		params.input_pipes[0] = pipe;
                //params.avidemux = true;
                
		cmdList.add(pipe.getInputPipe());

		String[] cmdArray = new String[cmdList.size()];
		cmdList.toArray(cmdArray);
                
                for(String ar: cmdArray){
                    System.out.print(ar+" ");
                }
                System.out.println();
		ProcessWrapperImpl pw = new ProcessWrapperImpl(cmdArray, params);
                
		ProcessWrapper mkfifo_process = pipe.getPipeProcess();
                System.out.println("mkfprocess:"+mkfifo_process.toString());
		pw.attachProcess(mkfifo_process);
		mkfifo_process.runInNewThread();
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
		}
		pipe.deleteLater();

		pw.runInNewThread();
                
                System.out.println("SCREEN: "+pipe.getPipeProcess());
                System.out.println("SCREEN: "+pw.toString());
		return pw;
	}
}

//./ffmpeg -y -loglevel debug -s 1920x1080 -f x11grab -i :0.0 -c:v mjpeg -ac 2 -f alsa -i pulse -qscale 0 -g 1 -f avi udp://localhost:48550