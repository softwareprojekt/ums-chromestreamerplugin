# UMS ChromeStreamerPlugin

This is the main repository for our Softwareproject.
There are several repositories, which we depend on and which you have to use.

## Install

First of all we have to clone this repository
just create a folder "streamer" 
```
mkdir streamer
cd streamer
```
Then you can clone this repository
```
git clone https://antonanders@bitbucket.org/softwareprojekt/ums-chromestreamerplugin.git
```
now there is a new folder inside.
You can switch inside by using 
```
cd ChromeStreamerPlugin
```

Now we can clone the repositories we need, install all dependencies we need and compile all sources into our binary-folder
```
./build.sh --with-ums --install-dependencies --resolve --copy
```

if you want to check if everything compiled ok, just run
```
./build.sh --run
```

## Development
Now you can start developing. You can use Netbeans for coding. The sources are compiled by maven.
To run your source, just type
```
./build.sh --run
```
The plugins sources are compiled and the binary is copied into the UMS/plugins folder. 
Then the server will be started automaticly.
