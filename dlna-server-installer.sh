#!/bin/sh

wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install vlc mplayer openjdk-7-jre google-chrome-stable

wget https://bitbucket.org/softwareprojekt/ums-chromestreamerplugin/downloads/DLNAStreamer.tar.gz 

 
tar -xvf $(ls |grep tar.gz) -C "."

if [ -e "./UMS.sh" ]; then
	sudo ln -s UMS.sh /bin/DLNAStreamer
fi

rm $(ls |grep tar.gz)
