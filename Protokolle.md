Protokoll für Streaming-Steuerung
===================

Diese Übersicht soll noch einmal kurz zusammenfassen, wie die Kommunikation zwischen den Streaming-Modulen abläuft.
> **Legende** 
A = App
S = Server
E = Extension



[TOC]

----------


App <=> Server
-------------

> Die Übertragung von der App zum Server erfolgt über GET-Parameter
> Die Rückrichtung hingegen ist ein JSON-Format

### **/click** 
A -> S :
```
x: [0..1000]
y: [0..1000]
key:["left", "right", "mid"] 
```
S -> A

```
{
	request:boolean	
}
```

### **/move** 
A -> S :
```
x:    [0..1000],
y:    [0..1000],
state:["clicked", "hover"] 
```
S -> A

```
{
	request:boolean	
}
```

### **/scroll** 
A -> S :
```
dx: [-100..100]
dy: [-100..100]
```
S -> A
```
request:boolean	
```

### **/zoom**
A -> S :
```
factor: [-100..+100]
```
S -> A
```
{
	request:boolean	
}
```

### **/getRenderer**
A -> S :
```

```
S -> A
```
[
	{
		uuid:"UUID-String",
		ip: "IP-String",
		name: "",
		vendor:"",
		icon:"Link to ICON"
	},
	...
]
```

### **/controllVideo**
A -> S :
```
key:["play", "pause", "stop", "fullscreen"]
```
S -> A
```
{
	request:boolean	
}
```


### **/controllRenderer**
A -> S :
```
key:["volume", "key"]
// if key == volume
value: [0..100]
// if key == key
value: ["play", "stop", "pause", "up", "down", "left", "right", "poweroff"]
// pause, up, down, left, right sind zum steuern des RendererMenüs.
// play muss ausgeführt werden, damit der DMR etwas abspielt(nach setRenderer ausführen)
```
S -> A
```
{
	request:boolean	
}
```
### **/getStatus**
A -> S :
```

```
S -> A
```
{
	status:["nobrowser", "idle", "video_playing", 
		"video_paused", "video_stopped", 
		"interacting", "notloaded"],
	target: {
		uuid:"UUID-String",
		ip: "IP-String",
		name: "",
		vendor:"",
		icon:"Link to ICON"
	}
}
```

### **/setURL**
A -> S :
```
url: "URL"
cookie: "Cookie"
```
S -> A
```
{
	request:boolean	
}
```
### **/setText**
A -> S :
```
text: "TEXT"
```
S -> A
```
{
	request:boolean	
}
```
### **/pageForward**
A -> S :
```

```
S -> A
```
{
	request:boolean	
}
```
### **/pageBack**
A -> S :
```

```
S -> A
```
{
	request:boolean	
}
```
### **/refreshPage**
A -> S :
```

```
S -> A
```
{
	request:boolean	
}
```

Server <=> Extension
--------------------
> Die Übertragung vom Server zur Extension und zurück erfolgt über ein Websocket im JSON-Format

### **click**
S -> E :
```
{
	function:"click",
	x:[0..1000],
	y:[0..1000],
	key:["left", "mid", "right"]
}
```
E -> S
```

```
### **move**
S -> E :
```
{
	function:"move",
	x:[0..1000],
	y:[0..1000],
	state:["clicked", "hover"]
}
```
E -> S
```

```

### **scroll**
S -> E :
```
{
	function:"scroll",
	dx:[-100..100]
	dy:[-100..100]
}
```
E -> S
```

```

### **zoom**
S -> E :
```
{
	function:"zoom",
	faktor:[-100..100]
}
```
E -> S
```

```

### **controllVideo**
S -> E :
```
{
	function:"controllVideo",
	key: ["play", "pause", "stop", "fullscreen"]	
}
```
E -> S
```
{	
	request:boolean,
	function:"controllVideo"
}
```


### **getInformation**
S -> E :
```
{
	function:"getInformation"	
}
```
E -> S
```
{
	request:boolean,
	function:"getInformation",
	playable_video:boolean,
	status: ["play", "pause", "stop"],
	loaded: boolean
}
```

### **sendURL**
S -> E :
```
{
	function:"sendURL",
	url:"URL"	
}
```
E -> S
```

```

### **sendText**
S -> E :
```
{
	function:"sendText",
	text:"TEXT"	
}
```
E -> S
```

```

### **pageForward**
S -> E :
```
{
	function:"pageForward"	
}
```
E -> S
```
{
	request:boolean,
	function:"pageForward",
	hasForward:boolean,
}
```

### **pageBack**
S -> E :
```
{
	function:"pageBack"	
}
```
E -> S
```
{
	request:boolean,
	function:"pageBack",
	hasBack:boolean,
}
```

### **refreshPage**
S -> E :
```
{
	function:"refreshPage"	
}
```
E -> S
```

```
