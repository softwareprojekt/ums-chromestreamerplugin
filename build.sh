#!/bin/sh

COMPILE_UMS=false
COPY_TO_UMS_DIR=false
RUN_UMS=false
RESOLVE_DEBENDENCIES=false
INSTALL_DEPENDENCIES=false
USER_NAME=antonanders
UMS_SOURCE_DIR="ums"
BINARY_DIR="../../binary/"
UMS_BINARY_DIR="ums"
UMS_BRANCH="softwareprojekt"

# STRUCTURE
# root
# |-binary
# |-ums
# |-ChromeStreamerPlugin

if [ "$#" -ne 0 ];   
then
	for var in "$@"
	do
    	#echo "$var"
		if [ "$var" = "--with-ums" ]; 
		then
			COMPILE_UMS=true
		fi

		if [ "$var" = "--run" ]; 
		then
			RUN_UMS=true
			COPY_TO_UMS_DIR=true
		fi

		if [ "$var" = "--copy" ]; 
		then
			COPY_TO_UMS_DIR=true
		fi

		if [ "$var" = "--resolve" ]; 
		then
			RESOLVE_DEBENDENCIES=true
		fi

		
		if [ "$var" = "--install-dependencies" ]; 
		then
			INSTALL_DEPENDENCIES=true
		fi
	done
fi

# Clone and pull Code, switch to branch and compile with maven
if ($COMPILE_UMS) then
	CWD=$(pwd)
	cd ..
	if [ ! -d "$UMS_SOURCE_DIR" ]; then
		git clone "https://$USER_NAME@bitbucket.org/softwareprojekt/universalmediaserver.git" ums
	fi
	cd ums
	
	#Checkout to Softwareprojekt-Branch and pull newest sources
	
	#git checkout -b "$UMS_BRANCH"
	git pull

	# compile with maven
	rm -r target # clear build

	if [ "$RESOLVE_DEBENDENCIES" = true ]; then
		mvn com.savage7.maven.plugins:maven-external-dependency-plugin:resolve-external
		mvn com.savage7.maven.plugins:maven-external-dependency-plugin:install-external
	fi

	if [ "$INSTALL_DEPENDENCIES" = true ]; then
	#	./install_dependencies.sh
		mvn install
	fi

	mvn package

	# copy build to binary
	cd target
	mkdir "$BINARY_DIR"
	#if [ -d "$BINARY_DIR$UMS_BINARY_DIR" ]; then
		rm -r "$BINARY_DIR$UMS_BINARY_DIR"
	#fi
	mkdir "$BINARY_DIR$UMS_BINARY_DIR"
	
	tar -xvf $(ls |grep tar.gz) -C "$BINARY_DIR$UMS_BINARY_DIR"
		

	cd $CWD
	
fi


# Build the Plugin
#if [ "$RESOLVE_DEBENDENCIES" = true ]; then
	#mvn com.savage7.maven.plugins:maven-external-dependency-plugin:resolve-external
	#mvn com.savage7.maven.plugins:maven-external-dependency-plugin:install-external
#fi


mvn package

#copy to plugins of UMS
if [ "$COPY_TO_UMS_DIR" = true ]; then
	cd target
	# Copy Plugin into UML-Plugin-Folder
	cp "ChromeStreamerPlugin-1.0-SNAPSHOT.jar" "$BINARY_DIR$UMS_BINARY_DIR/plugins/ChromeStreamerPlugin.jar"
fi

if [ "$RUN_UMS" = true ]; then
	# start UMS
	sh "$BINARY_DIR$UMS_BINARY_DIR/UMS.sh"
fi

