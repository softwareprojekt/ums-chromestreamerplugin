#!/bin/bash

IN="$(pacmd list-sources | grep analog-stereo.monitor)"
IFS="<>"
set -- $IN

echo "$2"

DEVICE="$2"

FILE="$HOME/.asoundrc"

echo "" > $FILE

echo "
pcm.pulse_monitor {
  type pulse
  device $DEVICE
}

ctl.pulse_monitor {
  type pulse
  device $DEVICE
}" > "$FILE"

